#include "io_util.h"

enum read_status from_bmp(FILE *in, struct image_t *image) {
    struct bmp_header_t header = {0};

    size_t header_read_result = fread(&header, sizeof(struct bmp_header_t), 1, in);

    /* Verify file was read */
    if (!header_read_result) {
        return READ_INVALID_HEADER;
    }

    /* Verify file is .bmp */
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    /* Verify biBitCount */
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    *image = image_t_create(header.biWidth, header.biHeight);
    if (image->data == NULL) {
        return READ_OUT_OF_MEMORY;
    }

    fseek(in, header.bOffBits, SEEK_SET);

    size_t padding = calculate_padding(header.biWidth);
    size_t pixel_size = sizeof(struct pixel_t);
    size_t pixel_index;
    for (size_t y = 0; y < header.biHeight; y++) {
        for (size_t x = 0; x < header.biWidth; x++) {
            pixel_index = pixel_index_by_coordinates(x, y, header.biWidth);
            if (!fread(
                    &image->data[pixel_index], pixel_size, 1, in)) {
                image_t_destroy(image);
                return READ_INVALID_CONTENT;
            }
        }
        fseek(in, (long) padding, SEEK_CUR);
    }

    return READ_OK;
}


enum write_status to_bmp(FILE *out, struct image_t const *image) {
    size_t padding = calculate_padding(image->width);
    size_t header_size = sizeof(struct bmp_header_t);
    size_t content_size = image->width * image->height * sizeof(struct pixel_t)
                          + padding * image->height;

    struct bmp_header_t header = (struct bmp_header_t) {
            .bfType = 0x4D42,
            .bfileSize = header_size + content_size,
            .bfReserved = 0,
            .bOffBits = header_size,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = content_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (!fwrite(&header, header_size, 1, out)) {
        return WRITE_HEADER_ERROR;
    }

    const uint32_t tmp = 0;
    size_t pixel_size = sizeof(struct pixel_t);
    size_t pixel_index;
    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            pixel_index = pixel_index_by_coordinates(x, y, image->width);
            if (!fwrite(&image->data[pixel_index], pixel_size, 1, out)) {
                return WRITE_CONTENT_ERROR;
            }
        }

        if (!fwrite(&tmp, padding, 1, out))
            return WRITE_CONTENT_ERROR;

    }

    return WRITE_OK;
}

enum open_file_status open_file(FILE **file, char *path, char *mode) {
    *file = fopen(path, mode);
    if (*file != NULL) {
        return OPEN_OK;
    }
    return OPEN_ERROR;
}

enum close_file_status close_file(FILE *file) {
    if (!fclose(file)) {
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}

char *get_read_status(enum read_status code) {
    switch (code) {
        case READ_OK:
            return "read successfully";
        case READ_INVALID_SIGNATURE:
            return "invalid signature";
        case READ_INVALID_BITS:
            return "invalid bits";
        case READ_INVALID_HEADER:
            return "invalid header";
        case READ_INVALID_CONTENT:
            return "invalid content";
        case READ_OUT_OF_MEMORY:
            return "out of memory";
        default:
            return "undefined status code";
    }
}

char *get_write_status(enum write_status code) {
    switch (code) {
        case WRITE_OK:
            return "wrote successfully";
        case WRITE_HEADER_ERROR:
            return "could not write header";
        case WRITE_CONTENT_ERROR:
            return "could not write content";
        default:
            return "undefined status code";
    }
}

char *get_open_file_status(enum open_file_status code) {
    switch (code) {
        case OPEN_OK:
            return "opened successfully";
        case OPEN_ERROR:
            return "could not open the file";
        default:
            return "undefined status code";
    }
}

char *get_close_file_status(enum close_file_status code) {
    switch (code) {
        case CLOSE_OK:
            return "closed successfully";
        case CLOSE_ERROR:
            return "could not close the file";
        default:
            return "undefined status code";
    }
}
