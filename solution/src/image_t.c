#include "image_t.h"

#include <malloc.h>

struct image_t image_t_create(uint64_t width, uint64_t height) {
    return (struct image_t) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel_t))
    };
}

void image_t_destroy(struct image_t *image) {
    free(image->data);
}

