#include "calc_util.h"

size_t pixel_index_by_coordinates(uint64_t x, uint64_t y, uint32_t width) {
    return x + y * width;
}

size_t calculate_padding(uint32_t width) {
    return (4 - width * sizeof(struct pixel_t) % 4) % 4;
}
