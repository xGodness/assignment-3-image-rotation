#include "exit_codes.h"
#include "image_t.h"
#include "io_util.h"
#include "rotate_bmp.h"

static void print_error(char *message) {
    printf("An error occurred: \n\t%s\n", message);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        print_error("invalid number of arguments");
        return INVALID_NUMBER_OF_ARGUMENTS;
    }

    FILE *input = NULL;

    enum open_file_status open_file_code = open_file(&input, argv[1], "r");
    if (open_file_code) {
        print_error(get_open_file_status(open_file_code));
        return ERROR_OPENING_FILE;
    }

    struct image_t src_image = {0};
    enum read_status read_status_code = from_bmp(input, &src_image);
    if (read_status_code) {
        print_error(get_read_status(read_status_code));
        return ERROR_READING_BMP_FILE;
    }

    enum close_file_status close_status_code = close_file(input);
    if (close_status_code) {
        print_error(get_close_file_status(close_status_code));
        return ERROR_CLOSING_FILE;
    }

    struct image_t target_image = rotate_bmp(src_image);
    if (target_image.data == NULL) {
        print_error("Could not rotate image");
        return ERROR_ROTATING_BMP;
    }


    FILE *output = NULL;

    open_file_code = open_file(&output, argv[2], "w");
    if (output == NULL) {
        print_error(get_open_file_status(open_file_code));
        return ERROR_OPENING_FILE;
    }

    enum write_status write_status_code = to_bmp(output, &target_image);
    if (write_status_code) {
        print_error(get_write_status(write_status_code));
        return ERROR_WRITING_TO_BMP_FILE;
    }

    close_status_code = close_file(output);
    if (close_status_code) {
        print_error(get_close_file_status(close_status_code));
        return ERROR_CLOSING_FILE;
    }

    image_t_destroy(&src_image);
    image_t_destroy(&target_image);

    return OK;
}
