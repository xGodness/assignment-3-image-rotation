#include "rotate_bmp.h"

struct image_t rotate_bmp(struct image_t src_image) {
    size_t target_width = src_image.height;
    size_t target_height = src_image.width;

    struct image_t target_image = image_t_create(target_width, target_height);

    if (target_image.data == NULL)
        return (struct image_t) {0};

    size_t src_pixel_index;
    size_t target_pixel_index;
    for (size_t y = 0; y < src_image.height; y++) {
        for (size_t x = 0; x < src_image.width; x++) {
            src_pixel_index = pixel_index_by_coordinates(x, y, src_image.width);
            target_pixel_index = pixel_index_by_coordinates(src_image.height - y - 1, x, target_width);
            target_image.data[target_pixel_index] = src_image.data[src_pixel_index];
        }
    }

    return target_image;
}
