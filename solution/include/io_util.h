#ifndef IO_UTIL_H
#define IO_UTIL_H

#include "bmp_header_t.h"
#include "calc_util.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_CONTENT,
    READ_OUT_OF_MEMORY
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_CONTENT_ERROR
};

enum open_file_status {
    OPEN_OK = 0,
    OPEN_ERROR = 1
};

enum close_file_status {
    CLOSE_OK = 0,
    CLOSE_ERROR = 1
};

enum read_status from_bmp(FILE *in, struct image_t *image);

enum write_status to_bmp(FILE *out, struct image_t const *image);

enum open_file_status open_file(FILE **file, char *path, char *mode);

enum close_file_status close_file(FILE *file);

char *get_read_status(enum read_status code);

char *get_write_status(enum write_status code);

char *get_open_file_status(enum open_file_status code);

char *get_close_file_status(enum close_file_status code);

#endif
