#ifndef IMAGE_T_H
#define IMAGE_T_H

#include <inttypes.h>

struct pixel_t {
    uint8_t b;
    uint8_t r;
    uint8_t g;
};

struct image_t {
    uint64_t width;
    uint64_t height;
    struct pixel_t *data;
};

struct image_t image_t_create(uint64_t width, uint64_t height);

void image_t_destroy(struct image_t *image);

#endif
