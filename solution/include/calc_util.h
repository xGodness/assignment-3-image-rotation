#ifndef CALC_UTIL_H
#define CALC_UTIL_H

#include "image_t.h"

#include <inttypes.h>
#include <stdio.h>

size_t pixel_index_by_coordinates(uint64_t x, uint64_t y, uint32_t width);

size_t calculate_padding(uint32_t width);

#endif
