#ifndef ROTATE_BMP_H
#define ROTATE_BMP_H

#include "calc_util.h"
#include "image_t.h"

#include <stdio.h>

struct image_t rotate_bmp(struct image_t src_image);

#endif
