#ifndef EXIT_CODES_H
#define EXIT_CODES_H

enum exit_codes {
    OK = 0,
    INVALID_NUMBER_OF_ARGUMENTS = 1,
    ERROR_OPENING_FILE = 2,
    ERROR_CLOSING_FILE = 3,
    ERROR_READING_BMP_FILE = 4,
    ERROR_WRITING_TO_BMP_FILE = 5,
    ERROR_ROTATING_BMP = 6
};

#endif
